import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { MaterialModule } from 'apps/ilmstudios/src/app/modules/material/material.module';
@NgModule({
  imports: [CommonModule, MaterialModule],
  declarations: [LandingPageComponent]
})
export class SharedComponentsModule {}
