import { Masjid } from '../Masjid/masjid';

export class PrayerUpdate {
  public id: string;
  public masjidId: string;
  public masjidName: string;
  public prayer: string;
  public time: string;
  public userId: string;
  public status: StatusEnum;
  constructor(masjid: Masjid, prayer: string, time: string) {
    this.masjidId = masjid.id;
    this.masjidName = masjid.name;
    this.prayer = prayer;
    this.time = time;
  }
}

export enum StatusEnum {
  Pending = 0,
  Approved = 1,
  Denied = 2
}
