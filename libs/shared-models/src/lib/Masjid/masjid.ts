export interface Masjid {
  id: string;
  name: string;
  fajr: string;
  zuhr: string;
  asr: string;
  isha: string;
  jummah: string;
  jummah2: string;
  address: string;
}
