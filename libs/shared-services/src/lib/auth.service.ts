// import { Injectable } from '@angular/core';
// import { Router } from '@angular/router';

// import { auth } from 'firebase';
// import { AngularFireAuth } from 'angularfire2/auth';
// import {
//   AngularFirestore,
//   AngularFirestoreDocument
// } from 'angularfire2/firestore';

// import { Observable, of } from 'rxjs';
// import { switchMap, take, first } from 'rxjs/operators';

// interface UserDetails {
//   uid: string;
//   email: string;
//   admin: boolean;
// }

// @Injectable()
// export class AuthService {
//   currentUser: UserDetails = null;
//   loggedIn = false;
//   isAdmin = false;

//   constructor(
//     private afAuth: AngularFireAuth,
//     private afs: AngularFirestore,
//     private router: Router
//   ) {
//     this.afAuth.user.subscribe(user => {
//       if (user !== null) {
//         this.updateUserData(user);
//       }
//     });
//   }

//   googleLogin() {
//     const provider = new auth.GoogleAuthProvider();
//     return this.oAuthLogin(provider);
//   }

//   private oAuthLogin(provider: any) {
//     return this.afAuth.auth.signInWithPopup(provider).then(credential => {
//       return this.updateUserData(credential.user);
//     });
//   }

//   emailLogin(email: string, password: string) {
//     return this.afAuth.auth
//       .signInWithEmailAndPassword(email, password)
//       .then(credential => {
//         return this.updateUserData(credential.user);
//       });
//   }

//   private updateUserData(user) {
//     // Sets user data to firestore on login

//     const userDetailsRef: AngularFirestoreDocument<UserDetails> = this.afs.doc(
//       `users/${user.uid}`
//     );

//     userDetailsRef.valueChanges().subscribe(userDetails => {
//       this.currentUser = userDetails as UserDetails;
//       this.isAdmin = this.currentUser.admin;
//     });

//     const data = {
//       uid: user.uid,
//       email: user.email,
//       admin: this.currentUser.admin || false
//     };

//     this.currentUser = user as UserDetails;
//     this.loggedIn = true;
//     return userDetailsRef.set(data as UserDetails, { merge: true });
//   }

//   signOut() {
//     this.afAuth.auth.signOut().then(() => {
//       this.currentUser = null;
//       this.loggedIn = false;
//       this.isAdmin = false;
//       this.router.navigate(['/']);
//     });
//   }
// }

import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { firebase } from '@firebase/app';
import { auth } from 'firebase';
import { AngularFireAuth } from 'angularfire2/auth';
import {
  AngularFirestore,
  AngularFirestoreDocument
} from 'angularfire2/firestore';

import { Observable, of } from 'rxjs';
import { switchMap, startWith, tap, filter } from 'rxjs/operators';

interface User {
  uid: string;
  email?: string | null;
}

export class UserDetails implements User {
  public uid: string;
  public email: string;
  public admin: boolean;
}

@Injectable()
export class AuthService {
  user: Observable<User | null>;
  currentUser: UserDetails;

  constructor(
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private router: Router
  ) {
    this.user = this.afAuth.authState.pipe(
      switchMap(user => {
        if (user) {
          return this.afs.doc<User>(`users/${user.uid}`).valueChanges();
        } else {
          return of(null);
        }
      })
      // startWith(JSON.parse(localStorage.getItem('user')))
    );
  }

  ////// OAuth Methods /////
  googleLogin() {
    const provider = new auth.GoogleAuthProvider();
    return this.oAuthLogin(provider);
  }

  private oAuthLogin(provider: any) {
    return this.afAuth.auth
      .signInWithPopup(provider)
      .then(credential => {
        return this.updateUserData(credential.user);
      })
      .catch(error => this.handleError(error));
  }

  signOut() {
    this.afAuth.auth.signOut().then(() => {
      this.currentUser = null;
      this.router.navigate(['/']);
    });
  }

  // If error, console log and notify user
  private handleError(error: Error) {
    console.error(error);
  }

  // Sets user data to firestore after succesful login
  private updateUserData(user: User) {
    const userRef: AngularFirestoreDocument<UserDetails> = this.afs.doc(
      `users/${user.uid}`
    );

    userRef.snapshotChanges().subscribe(response => {
      if (
        response.payload.data() !== null &&
        response.payload.data() !== undefined
      ) {
        const data: UserDetails = {
          uid: user.uid,
          email: user.email || null,
          admin: response.payload.data().admin || false
        };

        this.currentUser = data;

        userRef.set(data);

        this.router.navigate(['/']);
      } else {
        const data: UserDetails = {
          uid: user.uid,
          email: user.email || null,
          admin: false
        };

        this.currentUser = data;

        userRef.set(data);

        this.router.navigate(['/']);
      }
    });
  }

  public isLoggedIn() {
    return this.currentUser != undefined || this.currentUser != null;
  }

  public isAdmin() {
    return this.isLoggedIn() && this.currentUser.admin;
  }
}
