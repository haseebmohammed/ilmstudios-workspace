import { AuthService } from './auth.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GetAllCollectionService } from './get-all-collection.service';
import { GetSingleDocumentService } from './get-single-document.service';
@NgModule({
  imports: [CommonModule],
  providers: [GetAllCollectionService, GetSingleDocumentService, AuthService]
})
export class SharedServicesModule {}
