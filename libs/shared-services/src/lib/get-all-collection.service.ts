import { Injectable } from '@angular/core';
import { map, take } from 'rxjs/operators';
import { Observable } from 'rxjs';
import {
  AngularFirestore,
  AngularFirestoreCollection
} from 'angularfire2/firestore';

@Injectable({
  providedIn: 'root'
})
export class GetAllCollectionService {
  constructor(private readonly afs: AngularFirestore) {
    afs.firestore.settings({ timestampsInSnapshots: true });
  }

  getCollection(collectionName) {
    const itemsCollection = this.afs.collection<any>(collectionName);
    const items = itemsCollection.snapshotChanges().pipe(
      take(1),
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data() as any;
          const id = (a.payload.doc as any).id;
          return { id, ...data };
        });
      })
    );
    return items;
  }

  getCollectionWithOneFieldQuery(collectionName, columnName, expectedValue) {
    const itemsCollection = this.afs.collection<any>(collectionName, ref =>
      ref.where(columnName, '==', expectedValue)
    );
    const items = itemsCollection.snapshotChanges().pipe(
      take(1),
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data() as any;
          const id = (a.payload.doc as any).id;
          return { id, ...data };
        });
      })
    );
    return items;
  }
}
