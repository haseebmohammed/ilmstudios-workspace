import { map, take } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';

@Injectable({
  providedIn: 'root'
})
export class GetSingleDocumentService {
  constructor(private readonly afs: AngularFirestore) {
    afs.firestore.settings({ timestampsInSnapshots: true });
  }

  get(collectionName, documentId) {
    return this.afs
      .doc(collectionName + '/' + documentId)
      .snapshotChanges()
      .pipe(
        take(1),
        map(a => {
          const data = a.payload.data() as any;
          const id = a.payload.id;
          return { id, ...data };
        })
      );
  }

  getAsPromise(collectionName, documentId) {
    return this.afs
      .doc(collectionName + '/' + documentId)
      .snapshotChanges()
      .pipe(
        take(1),
        map(a => {
          const data = a.payload.data() as any;
          const id = a.payload.id;
          return { id, ...data };
        })
      )
      .toPromise();
  }
}
