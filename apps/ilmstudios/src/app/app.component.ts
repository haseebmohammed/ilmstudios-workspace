import { Component } from '@angular/core';

@Component({
  selector: 'ilmstudios-workspace-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ilmstudios';
}
