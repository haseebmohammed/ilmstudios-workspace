import { GetSingleDocumentService } from 'libs/shared-services/src/lib/get-single-document.service';
import { PrayerUpdate } from 'libs/shared-models/src/lib/PrayerUpdate/prayer-update';
import { Injectable, OnInit } from '@angular/core';
import { GetAllCollectionService } from 'libs/shared-services/src/lib/get-all-collection.service';
import {
  AngularFirestore,
  AngularFirestoreDocument
} from 'angularfire2/firestore';
import { AuthService } from 'libs/shared-services/src/lib/auth.service';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { Masjid } from 'libs/shared-models/src/lib/Masjid/masjid';

@Injectable({
  providedIn: 'root'
})
export class MasjidService implements OnInit {
  constructor(
    private getAllCollection: GetAllCollectionService,
    private getSingleDocumentService: GetSingleDocumentService,
    private readonly afs: AngularFirestore,
    private auth: AuthService,
    public snackBar: MatSnackBar,
    private router: Router
  ) {}

  ngOnInit() {}

  getAllMasjids() {
    return this.getAllCollection.getCollection('masjids');
  }

  submitUpdate(prayerUpdate: PrayerUpdate) {
    prayerUpdate.userId = this.auth.currentUser.uid;
    prayerUpdate.status = 0;
    this.afs
      .collection('prayerUpdates')
      .add(JSON.parse(JSON.stringify(prayerUpdate)))
      .then(response => {
        // console.log(response);
        if (this.auth.isLoggedIn() && this.auth.currentUser.admin) {
          prayerUpdate.id = response.id;
          this.updateTime(prayerUpdate);
        } else {
          this.snackBar.open('Successly submitted update!', '', {
            duration: 1000
          });
        }
        this.router.navigateByUrl('/');
      })
      .catch(error => {
        // console.log(error);
        this.snackBar.open('Error!', '', { duration: 1000 });
      });
  }

  getAllPendingPrayerUpdates() {
    return this.getAllCollection.getCollectionWithOneFieldQuery(
      'prayerUpdates',
      'status',
      0
    );
  }

  getMasjidName(masjidId): Promise<Masjid> {
    return this.getSingleDocumentService.getAsPromise('masjids', masjidId);
  }

  updateTime(prayerUpdate: PrayerUpdate) {
    // Update time on masjid
    const masjidRef: AngularFirestoreDocument<Masjid> = this.afs.doc(
      `masjids/${prayerUpdate.masjidId}`
    );
    const masjidData = {};
    const prayerName = prayerUpdate.prayer.toLowerCase();
    masjidData[prayerName] = prayerUpdate.time;
    masjidRef.update(masjidData as Masjid);

    // Update approval
    const updateRef: AngularFirestoreDocument<PrayerUpdate> = this.afs.doc(
      `prayerUpdates/${prayerUpdate.id}`
    );
    const prayerUpdateData = {};
    prayerUpdateData['status'] = 1;
    updateRef.update(prayerUpdateData as PrayerUpdate);

    this.snackBar.open('Successly approved update!', '', {
      duration: 1000
    });
    this.router.navigateByUrl('/');
  }

  denyTime(prayerUpdate: PrayerUpdate) {
    const updateRef: AngularFirestoreDocument<PrayerUpdate> = this.afs.doc(
      `prayerUpdates/${prayerUpdate.id}`
    );
    const prayerUpdateData = {};
    prayerUpdateData['status'] = 2;
    updateRef.update(prayerUpdateData as PrayerUpdate);

    this.snackBar.open('Successly denied update!', '', {
      duration: 1000
    });
    this.router.navigateByUrl('/');
  }

  addMasjidData() {
    masjids.forEach(masjid => {
      const updateRef: AngularFirestoreDocument<Masjid> = this.afs.doc(
        `masjids/${masjid.id}`
      );

      const masjidUpdate = {};
      masjidUpdate['address'] = masjid.address;
      updateRef.update(masjidUpdate as Masjid);
    });
  }
}

export const masjids: any[] = [
  {
    id: 'b7apnyMRPcO2XIQUZo8x',
    asr: '5:00',
    fajr: '6:15',
    isha: '8:30',
    jummah: '',
    jummah2: '',
    name: 'Tahoora Masjid',
    zuhr: '1:30',
    address: '6534 N Seeley Ave'
  },
  {
    id: 'if5MGTdT9yImFaTpkF5f',
    asr: '5:00',
    fajr: '6:15',
    isha: '8:30',
    jummah: '1:30',
    jummah2: '',
    name: 'Rahmat E Alam (W)',
    zuhr: '1:15',
    address: '7045 N Western Ave'
  },
  {
    id: 'Pz5iuvQZYu9g5GWhrUEF',
    asr: '5:00',
    fajr: '6:15',
    isha: '8:20',
    jummah: '1:15',
    jummah2: '',
    name: 'Rahmat E Alam (C)',
    zuhr: '1:15',
    address: '6201 N California Ave'
  },
  {
    id: 'JL1gynfGLc0KcuQyAj52',
    asr: '5:15',
    fajr: '5:45',
    isha: '8:30',
    jummah: '1:30',
    jummah2: '',
    name: 'Masjid Taqwa',
    zuhr: '1:30',
    address: '5115 N Sheridan Rd'
  },
  {
    id: 'AJulaO70CynNjIlf9ClQ',
    asr: '5:30',
    fajr: '6:00',
    isha: '8:15',
    jummah: '2:10',
    jummah2: '',
    name: 'Masjid Rahmat',
    zuhr: '1:15',
    address: '6412 N Talman Ave'
  },
  {
    id: 'p0D8oTA3zCe8r7slvodt',
    asr: '5:00',
    fajr: '6:15',
    isha: '8:15',
    jummah: '1:30',
    jummah2: '',
    name: 'Masjid E Suffah',
    zuhr: '1:30',
    address: '8201 Karlov Ave'
  },
  {
    id: 'esPDVWEP1kD7QIQy9J5m',
    asr: '5:30',
    fajr: '6:00',
    isha: '8:15',
    jummah: '1:30',
    jummah2: '',
    name: 'Masjid E Noor',
    zuhr: '1:30',
    address: '6151 N Greenview Ave'
  },
  {
    id: 'WgVtTwuGK6oNkVFbkuYa',
    asr: '5:00',
    fajr: '6:15',
    isha: '8:15',
    jummah: '1:30',
    jummah2: '',
    name: 'Masjid Abu Bakr',
    zuhr: '1:30',
    address: '1017 W Roscoe St'
  },
  {
    id: 'XTEvT6tLAYs99Psgp0aH',
    asr: '5:15',
    fajr: '6:00',
    isha: '8:35',
    jummah: '1:30',
    jummah2: '3:00',
    name: 'Makki Masjid',
    zuhr: '1:30',
    address: '3418 W Ainslie St'
  },
  {
    id: 'bDKy5NQ9FzVF4EEr1D3i',
    asr: '5:30',
    fajr: '6:00',
    isha: '9:00',
    jummah: '1:15',
    jummah2: '2:15',
    name: 'M.E.C',
    zuhr: '1:10',
    address: '8601 N. Menard Ave'
  },
  {
    id: 'htNBCczNXZiqPfZNGidJ',
    asr: '5:30',
    fajr: '5:45',
    isha: '9:00',
    jummah: '1:05',
    jummah2: '1:40',
    name: 'M.C.C',
    zuhr: '1:30',
    address: '4380 N. Elston Ave'
  },
  {
    id: 'ygZKUqLpee79iFlXRnYa',
    asr: '5:30',
    fajr: '6:05',
    isha: '8:20',
    jummah: '2:00',
    jummah2: '',
    name: 'LMCC',
    zuhr: '2:00',
    address: '2150 W Devon Ave, #1'
  },
  {
    id: 'chYr6JIKt38bOu3EG1jg',
    asr: '5:00',
    fajr: '6:15',
    isha: '8:15',
    jummah: '1:30',
    jummah2: '',
    name: 'Jama Masjid',
    zuhr: '1:30',
    address: '6240 N Campbell Ave'
  },
  {
    id: 'fxXdO6iGwRcfdpNkweV3',
    asr: '5:15',
    fajr: '6:20',
    isha: '7:50',
    jummah: '1:30',
    jummah2: '',
    name: 'ICNA',
    zuhr: '1:00',
    address: '6224 N California Ave'
  },
  {
    id: 'kx0Z0xDaUl2bSQ6Zonke',
    asr: '5:15',
    fajr: '6:15',
    isha: '8:15',
    jummah: '1:10',
    jummah2: '1:40',
    name: 'I.C.C.',
    zuhr: '1:30',
    address: '5933 N Lincoln Ave'
  },
  {
    id: 'GSFau92KoK51hsHHteSf',
    asr: '5:00',
    fajr: '6:15',
    isha: '8:00',
    jummah: '1:30',
    jummah2: '',
    name: 'Faizan Ul Quran',
    zuhr: '1:45',
    address: '6412 N Hamilton Ave'
  },
  {
    id: 'lRiO6kSodVFE03Dy5deG',
    asr: '4:15',
    fajr: '5:45',
    isha: '8:00',
    jummah: '12:30',
    jummah2: '',
    name: 'Darut Tawheed ',
    zuhr: '1:15',
    address: '6351 N Western Ave'
  },
  {
    id: 'Fdf3trlwYGakkaUlj7on',
    asr: '5:30',
    fajr: '6:00',
    isha: '8:30',
    jummah: '1:15',
    jummah2: '',
    name: 'Darus Sunnah',
    zuhr: '1:30',
    address: '2045 Brown Ave'
  },
  {
    id: 'jth7fw4ioaABJOoOqeb3',
    asr: '4:45',
    fajr: '6:00',
    isha: '8:30',
    jummah: '1:00',
    jummah2: '',
    name: 'Darul Quran',
    zuhr: '1:15',
    address: '2514 W Thorndale Ave'
  },
  {
    id: 'YPgAn8ETBDDvFjud2Cny',
    asr: '5:30',
    fajr: '6:15',
    isha: '8:15',
    jummah: '2:00',
    jummah2: '3:15',
    name: 'Darul Emaan',
    zuhr: '2:00',
    address: '6355 N Claremont Ave'
  },
  {
    id: 'crEralEaWBzAlcPw7QJT',
    asr: '4:30',
    fajr: '6:00',
    isha: '8:00',
    jummah: '1:30',
    jummah2: '',
    name: 'Chicago Mosque',
    zuhr: '1:00',
    address: '6201 W Peterson Ave'
  },
  {
    id: 'pJjFOQwBpsDZpqKIrERw',
    asr: '6:00',
    fajr: '5:50',
    isha: '9:15',
    jummah: '',
    jummah2: '',
    name: 'Al Falah',
    zuhr: '1:30',
    address: '6404 N Fairfield Ave'
  }
];
