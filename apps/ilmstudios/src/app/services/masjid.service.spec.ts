import { TestBed, inject } from '@angular/core/testing';

import { MasjidService } from './masjid.service';

describe('MasjidService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MasjidService]
    });
  });

  it('should be created', inject([MasjidService], (service: MasjidService) => {
    expect(service).toBeTruthy();
  }));
});
