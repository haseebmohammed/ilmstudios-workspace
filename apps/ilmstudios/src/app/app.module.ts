import { AuthService } from 'libs/shared-services/src/lib/auth.service';
import { RouterModule } from '@angular/router';
import { ComponentsModule } from './components/components.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NxModule } from '@nrwl/nx';
import { AppRoutingModule } from './app.routing.module';
import { SharedComponentsModule } from '@ilmstudios-workspace/shared-components';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './modules/material/material.module';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { environment } from '../environments/environment.prod';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AuthGuard } from 'libs/shared-guards/src/lib/auth.guard';
import { AdminGuard } from 'libs/shared-guards/src/lib/admin.guard';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    NxModule.forRoot(),
    AppRoutingModule,
    SharedComponentsModule,
    BrowserAnimationsModule,
    ComponentsModule,
    MaterialModule,
    AngularFireModule.initializeApp(environment.firebase, 'ilm-studios'),
    AngularFirestoreModule,
    AngularFireAuthModule,
    HttpClientModule
  ],
  providers: [AuthService, AuthGuard, AdminGuard],
  bootstrap: [AppComponent]
})
export class AppModule {}
