import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingPageComponent } from 'libs/shared-components/src/lib/landing-page/landing-page.component';
import { PrayerTimesComponent } from './components/prayer-times/prayer-times.component';
import { SubmitUpdateComponent } from './components/submit-update/submit-update.component';
import { LoginComponent } from './components/login/login.component';
import { AuthGuard } from 'libs/shared-guards/src/lib/auth.guard';
import { PendingUpdatesComponent } from './components/pending-updates/pending-updates.component';
import { AdminGuard } from 'libs/shared-guards/src/lib/admin.guard';
import { FeatureDevelopmentComponent } from './components/feature-development/feature-development.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'prayer-times'
  },
  {
    path: 'home',
    component: LandingPageComponent
  },
  {
    path: 'prayer-times',
    component: PrayerTimesComponent
  },
  {
    path: 'submit-update',
    component: SubmitUpdateComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'pending-updates',
    component: PendingUpdatesComponent,
    canActivate: [AdminGuard]
  },
  {
    path: 'log-in',
    component: LoginComponent
  },
  {
    path: 'feature-development',
    component: FeatureDevelopmentComponent
  },
  {
    path: '*',
    redirectTo: 'prayer-times'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
