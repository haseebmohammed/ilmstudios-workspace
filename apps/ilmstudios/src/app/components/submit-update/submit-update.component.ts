import { Masjid } from 'libs/shared-models/src/lib/Masjid/masjid';
import { PrayerUpdate } from 'libs/shared-models/src/lib/PrayerUpdate/prayer-update';
import { Component, OnInit } from '@angular/core';
import { MasjidService } from '../../services/masjid.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'libs/shared-services/src/lib/auth.service';

@Component({
  selector: 'app-submit-update',
  templateUrl: './submit-update.component.html',
  styleUrls: ['./submit-update.component.css']
})
export class SubmitUpdateComponent implements OnInit {
  masjids: Masjid[] = [];
  prayers: string[] = ['Fajr', 'Zuhr', 'Asr', 'Isha', 'Jummah', 'Jummah2'];

  submitPrayerUpdateForm = new FormGroup({
    masjidId: new FormControl('', Validators.required),
    prayer: new FormControl('', Validators.required),
    time: new FormControl('', [
      Validators.required,
      Validators.pattern('(([1][0-2]|^[0]?[1-9]):[0-5]{1}[0-9]{1})')
    ])
  });

  constructor(private masjidService: MasjidService) {}

  ngOnInit() {
    this.masjidService.getAllMasjids().subscribe(masjidsResponse => {
      this.masjids = [];
      masjidsResponse.forEach(masjid => {
        this.masjids.push(JSON.parse(JSON.stringify(masjid)) as Masjid);
      });
    });
  }

  submitForm() {
    const prayerUpdate = this.submitPrayerUpdateForm.value as PrayerUpdate;
    prayerUpdate.masjidName = this.masjids.find(
      x => x.id == prayerUpdate.masjidId
    ).name;
    this.masjidService.submitUpdate(prayerUpdate);
  }
}
