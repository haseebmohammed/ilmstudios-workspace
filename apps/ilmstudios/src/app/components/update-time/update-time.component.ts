import { Component, OnInit, Inject } from '@angular/core';
import { Masjid } from 'libs/shared-models/src/lib/Masjid/masjid';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import * as moment from 'moment';

@Component({
  selector: 'app-update-time',
  templateUrl: './update-time.component.html',
  styleUrls: ['./update-time.component.css']
})
export class UpdateTimeComponent implements OnInit {
  saving = false;
  tempTime = '00:00';

  constructor(
    public dialogRef: MatDialogRef<UpdateTimeComponent>,
    @Inject(MAT_DIALOG_DATA) public data: MasjidUpdate
  ) {}

  ngOnInit() {
    this.tempTime = this.data.masjid[this.data.prayer.toLowerCase()];
  }

  onSave(): void {
    this.saving = true;
    this.data.masjid[this.data.prayer.toLowerCase()] = this.tempTime;
    this.dialogRef.close(true);
  }

  onCancel(): void {
    this.dialogRef.close(false);
  }

  add() {
    this.theMath(5);
  }

  subtract() {
    this.theMath(-5);
  }

  theMath(amount) {
    let prayerTime = moment.utc(this.tempTime, 'H:mm');
    prayerTime = prayerTime.add('minute', amount);
    console.log(prayerTime.format('H:mm'));
    this.tempTime = prayerTime.format('H:mm');
  }
}

export class MasjidUpdate {
  masjid: Masjid;
  prayer: string;
}
