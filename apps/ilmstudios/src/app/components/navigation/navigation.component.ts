import { Component, ViewChild } from '@angular/core';
import {
  BreakpointObserver,
  Breakpoints,
  BreakpointState
} from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthService } from 'libs/shared-services/src/lib/auth.service';
import { MatSidenav } from '@angular/material';
import { Router, NavigationStart } from '@angular/router';
import * as moment from 'moment';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent {
  todaysDateAndSunset = "Today's sunset: ";

  @ViewChild('drawer') drawer: MatSidenav;
  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(map(result => result.matches));

  constructor(
    private breakpointObserver: BreakpointObserver,
    public auth: AuthService,
    private router: Router,
    private http: HttpClient
  ) {
    this.getSunsetTime();
    router.events.forEach(event => {
      if (event instanceof NavigationStart) {
        this.isHandset$.subscribe(response => {
          if (response) {
            this.drawer.close();
          }
        });
      }
      // NavigationEnd
      // NavigationCancel
      // NavigationError
      // RoutesRecognized
    });
  }

  toggleNav(drawer: MatSidenav) {
    drawer.toggle();
  }

  getSunsetTime() {
    this.http
      .get(
        'https://api.sunrise-sunset.org/json?lat=41.878113&lng=-87.629799&date=today'
      )
      .subscribe(r => {
        const response = r as any;
        this.todaysDateAndSunset =
          this.todaysDateAndSunset +
          moment(moment.utc(response.results.sunset, 'h:mm A').toDate()).format(
            'MM/DD/YYYY h:mm A'
          );
      });
  }
}
