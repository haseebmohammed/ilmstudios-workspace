import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeatureDevelopmentComponent } from './feature-development.component';

describe('FeatureDevelopmentComponent', () => {
  let component: FeatureDevelopmentComponent;
  let fixture: ComponentFixture<FeatureDevelopmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeatureDevelopmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeatureDevelopmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
