import { MaterialModule } from './../modules/material/material.module';
import { NavigationComponent } from './navigation/navigation.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrayerTimesComponent } from './prayer-times/prayer-times.component';
import { RouterModule } from '@angular/router';
import { SubmitUpdateComponent } from './submit-update/submit-update.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { PendingUpdatesComponent } from './pending-updates/pending-updates.component';
import { FeatureDevelopmentComponent } from './feature-development/feature-development.component';
import { UpdateTimeComponent } from './update-time/update-time.component';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    NavigationComponent,
    PrayerTimesComponent,
    SubmitUpdateComponent,
    LoginComponent,
    PendingUpdatesComponent,
    FeatureDevelopmentComponent,
    UpdateTimeComponent
  ],
  exports: [NavigationComponent, PrayerTimesComponent, SubmitUpdateComponent],
  entryComponents: [UpdateTimeComponent]
})
export class ComponentsModule {}
