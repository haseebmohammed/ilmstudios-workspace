import { Component, OnInit } from '@angular/core';
import { AuthService } from 'libs/shared-services/src/lib/auth.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm = new FormGroup({
    email: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required)
  });

  constructor(public auth: AuthService, private router: Router) {}

  ngOnInit() {}

  // emailLogin() {
  //   const loginForm = this.loginForm.value as Login;
  //   this.auth.emailLogin(loginForm.email, loginForm.password).then(response => {
  //     this.router.navigateByUrl('/prayer-times');
  //   });
  // }
}

export class Login {
  email: string;
  password: string;
}
