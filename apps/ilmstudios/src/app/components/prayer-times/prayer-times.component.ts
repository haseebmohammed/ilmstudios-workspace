import { Component, OnInit } from '@angular/core';
import { MatTableDataSource, MatDialog } from '@angular/material';
import { GetAllCollectionService } from 'libs/shared-services/src/lib/get-all-collection.service';
import { MasjidService } from '../../services/masjid.service';
import { Masjid } from 'libs/shared-models/src/lib/Masjid/masjid';
import { AuthService } from 'libs/shared-services/src/lib/auth.service';
import { UpdateTimeComponent } from '../update-time/update-time.component';
import { PrayerUpdate } from 'libs/shared-models/src/lib/PrayerUpdate/prayer-update';
import * as moment from 'moment';
import {
  animate,
  state,
  style,
  transition,
  trigger
} from '@angular/animations';

@Component({
  selector: 'app-prayer-times',
  templateUrl: './prayer-times.component.html',
  styleUrls: ['./prayer-times.component.css'],
  animations: [
    trigger('detailExpand', [
      state(
        'collapsed',
        style({ height: '0px', minHeight: '0', display: 'none' })
      ),
      state('expanded', style({ height: '*' })),
      transition(
        'expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')
      )
    ])
  ]
})
export class PrayerTimesComponent implements OnInit {
  displayedColumns = [
    'expand',
    'name',
    'fajr',
    'zuhr',
    'asr',
    'isha',
    'jummah'
  ];
  rawData: Masjid[] = [];
  dataSource = new MatTableDataSource(this.rawData);
  expandedElement: Masjid;

  constructor(
    private masjidService: MasjidService,
    public auth: AuthService,
    public dialog: MatDialog
  ) {}

  ngOnInit() {
    // this.masjidService.addMasjidData();
    this.masjidService.getAllMasjids().subscribe(masjidsResponse => {
      this.rawData = [];
      masjidsResponse.forEach(masjid => {
        this.rawData.push(JSON.parse(JSON.stringify(masjid)) as Masjid);
      });
      this.sortAlphabetically();
      this.dataSource = new MatTableDataSource(this.rawData);
      console.log(JSON.stringify(this.rawData));
    });
  }

  sortAlphabetically() {
    this.rawData = this.rawData.sort((a, b) => {
      return a.name > b.name ? -1 : 1;
    });
  }

  openDialog(masjid: Masjid, prayer: string) {
    const dialogRef = this.dialog.open(UpdateTimeComponent, {
      data: { masjid: masjid, prayer: prayer }
    });

    dialogRef.afterClosed().subscribe(response => {
      if (response) {
        this.masjidService.submitUpdate(
          new PrayerUpdate(
            masjid,
            prayer.toLowerCase(),
            masjid[prayer.toLowerCase()]
          )
        );
      }
    });
  }
}
