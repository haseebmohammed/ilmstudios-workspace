import { Component, OnInit } from '@angular/core';
import { PrayerUpdate } from 'libs/shared-models/src/lib/PrayerUpdate/prayer-update';
import { MasjidService } from '../../services/masjid.service';
import { MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-pending-updates',
  templateUrl: './pending-updates.component.html',
  styleUrls: ['./pending-updates.component.css']
})
export class PendingUpdatesComponent implements OnInit {
  displayedColumns: string[] = ['masjid', 'prayer', 'time', 'approve', 'deny'];
  rawData: PrayerUpdate[] = [];
  dataSource = new MatTableDataSource(this.rawData);

  constructor(private masjidService: MasjidService) {}

  ngOnInit() {
    this.masjidService
      .getAllPendingPrayerUpdates()
      .subscribe(masjidsResponse => {
        this.rawData = [];
        masjidsResponse.forEach(prayerUpdates => {
          let tempPrayerUpdate = JSON.parse(
            JSON.stringify(prayerUpdates)
          ) as PrayerUpdate;

          this.rawData.push(tempPrayerUpdate);
        });
        this.dataSource = new MatTableDataSource(this.rawData);
      });
  }

  approve(prayerUpdate) {
    this.masjidService.updateTime(prayerUpdate);
    this.rawData.splice(
      this.rawData.findIndex(x => x.id == prayerUpdate.id),
      1
    );
    this.dataSource = new MatTableDataSource(this.rawData);
  }

  deny(prayerUpdate) {
    this.masjidService.denyTime(prayerUpdate);
    this.rawData.splice(
      this.rawData.findIndex(x => x.id == prayerUpdate.id),
      1
    );
    this.dataSource = new MatTableDataSource(this.rawData);
  }
}
